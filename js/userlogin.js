$(document).ready(function () {
    $("#signup").submit(function (e) { 
        e.preventDefault();
        console.log($('#signup').serialize());
        var mail = $("[name=mail]")
        var pass = $("[name=pass]")
        var type = $("[name=type").val();
        $.ajax({
            type: "post",
            url: "php/validatelogin.php",
            data: $('#signup').serialize(),
            success: function (response) {
                var temp = JSON.parse(response);
                console.log(temp);
                if(temp.flag == true) {
                    mail.val('');
                    pass.val('');
                    if(type == "admin") {
                        window.open("admin.student.approval.php", "_self");
                    } else if(type=="faculty") {
                        window.open("facultyprofile.php", "_self");
                    } else {
                        window.open("userprofile.php","_self");
                    }
                    $("#error").removeClass("alert alert-danger");
                    $("#error").text("");
                } else {
                    $("#error").addClass("alert alert-danger");
                    $("#error").text("Enter valid Credentials");
                }
            },
            crossDomain:true
        });
    });
});
