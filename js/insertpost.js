$("#postform").submit(function (e) { 
    e.preventDefault();
    var data = new FormData(this);
    var image = $("#image")[0].files[0];
    var doc = $("#doc")[0].files[0];
    data.append('image',image);
    data.append('doc',doc);
    $.ajax({
        type: "POST",
        url: "php/insertpost.php",
        data: data,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (response) {
            if(response.flag == true) {
                alert('posted succesfully')
                $("#postmessage").val('');
                $("#image").val('');
                $("#doc").val('');
            } else {
                alert(response.flag);
            }
        }
    });
});