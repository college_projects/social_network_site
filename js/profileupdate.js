$(document).ready(function () {
    $("#update_profile").submit(function (e) { 
        e.preventDefault();
        var form = new FormData(this);
        var file = $("#image")[0].files[0];
        form.append('file',file);
        console.log(form);
        $.ajax({
            type: "post",
            url: "php/userupdate.php",
            data: form,
            contentType:false,
            processData:false,
            success: function (response) {
                var res = JSON.parse(response);
                if(res.status == true) {
                    $("#error").addClass("alert alert-success");
                    $("#error").text("Updated");
                    $("#pimg").attr("src",res.image);
                    $("#pname").text(res.name);
                    $("#pmail").text(res.gmail);
                    $("#ploc").text(res.location);
                    $("#pnum").text(res.number);
                    $("#pdob").text(res.date);
                    location.reload(true);
                } else {
                    $("#error").addClass("alert alert-danger");
                    $("#error").text("Failed");
                }
            }
        });
    });
    $("#update_faculty").submit(function (e) { 
        e.preventDefault();
        var form = new FormData(this);
        var file = $("#image")[0].files[0];
        form.append('file', file);
        $.ajax({
            type: "post",
            url: "php/facultyupdate.php",
            data: form,
            contentType: false,
            processData: false,
            dataType:"JSON",
            success: function (response) {
                console.log(response);
                var res = response;
                if (response.status == true) {
                    $("#error").addClass("alert alert-success");
                    $("#error").text("Updated");
                    $("#pimg").attr("src", res.image);
                    $("#pname").text(res.name);
                    $("#pmail").text(res.gmail);
                    $("#ploc").text(res.location);
                    $("#pnum").text(res.number);
                    $("#pdob").text(res.date);
                    location.reload(true);
                } else {
                    $("#error").addClass("alert alert-danger");
                    $("#error").text("Failed");
                }
            }
        });
    });
});