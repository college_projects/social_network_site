
var password = document.getElementById('password');
var confirm_password = document.getElementById('cpassword');
function validatePassword() {
    console.log(password,confirm_password)
    if (password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Password's Don't Match.");
    } else {
        confirm_password.setCustomValidity('');
    }
}
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
document.getElementById('submit').onclick = validatePassword;
