<?php
include('php/dbconnect.php');
$conn = new Connect();
$type = 'faculty';
$query = "select ud.userid,ud.name,pd.postmessage,pd.postimg,pd.postdoc,pd.postid from social.userdata as ud, social.posts as pd where pd.approved = false and ud.userid = pd.userid;";
$result = $conn->execute($query);
?>
<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>pending-approvals</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat+Alternates">
    <link rel="stylesheet" href="fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css" />
    <style>
        body {
            overflow: auto;
        }
    </style>
</head>

<body>
    <div>

    </div>
    <nav class="navbar navbar-dark navbar-expand-md bg-dark" style="max-width: 100%;">
        <div class="container"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#"><i class="fas fa-university"></i></a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Student</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item " href="admin.student.approval.php">Approval</a>
                            <a class="dropdown-item" href="admin.student.list.php">list</a>
                            <a class="dropdown-item active" href="admin.post.approval.php">Post Approval</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Faculty</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="admin.faculty.approval.php">Approval</a>
                            <a class="dropdown-item" href="admin.faculty.list.php">List</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Post</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="admin.post.php">Information</a>
                            <a class="dropdown-item" href="#">Student List</a>
                        </div>
                    </li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#"><i class="fa fa-power-off"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <br>
    <br>
    <div class="container">
        <table class="table table-info table-striped" id="Mytable">
            <thead class="thead thead-dark">
                <tr>
                    <th scope="col">StudentId</th>
                    <th scope="col">Name</th>
                    <th scope="col">Postmessage</th>
                    <th scope="col">Image</th>
                    <th scope="col">Document</th>
                    <th scope="col">Approve</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while ($row = $result->fetch_assoc()) {
                    $pid = $row['postid'];
                    $uid = $row['userid'];
                    $name = $row['name'];
                    $message = $row['postmessage'];
                    $img = $row['postimg'];
                    $doc  = $row['postdoc'];
                    if ($img == "") {
                        $img = "no image added";
                    }
                    if ($doc == "") {
                        $doc = "no document added";
                    }
                    echo "<tr id='$pid'>
                                <th scope='row'>$uid</th>
                                <td>$name</td>
                                <td>$message</td>
                                <td>$img</td>
                                <td>$doc</td>
                                <td><center><button class='btn btn-success' id='$pid' name='delete'>Approve</button></center></td>
                              </tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
    <script>
        var table = $("#Mytable").DataTable({
            dom: 'lBfrtip',
            buttons: [
                'excel', 'csv', 'copy'
            ],
            "lengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ]
        });
        $(document).on('click', '[name=delete]', function() {
            var id = $('[name=delete]').attr('id');
            $.ajax({
                type: "POST",
                url: "php/postapprove.php",
                data: {
                    'pid': id,
                },
                dataType: "JSON",
                success: function(response) {
                    console.log(response)
                    if (response.flag == true) {
                        var id = '#' + $('[name=delete]').attr('id');
                        table.row($(id))
                            .remove()
                            .draw();
                    }
                }
            });

        });
    </script>
</body>

</html>