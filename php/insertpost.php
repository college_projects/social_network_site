<?php
    session_start();
    $uid = $_SESSION['uid'];
    include('dbconnect.php');
    $conn = new Connect();
    $message = $_POST["postmessage"];
    $imgname = $_FILES['image']['name'];
    $imgpath = $_FILES['image']['tmp_name'];
    $docname = $_FILES['doc']['name'];
    $docpath = $_FILES['doc']['tmp_name'];
    $destimg = '../img/posts/'.$imgname;
    $destdoc = '../docs/' . $docname;
    $query = "select ud.name,ud.status from social.userdata as ud where ud.userid='$uid'";
    $flag = $conn->execute($query);
    $data = $flag->fetch_assoc();

    $approved = 0;
    if($data['status'] == 'faculty' or $data['status'] == 'admin') {
        $approved = true;
    }
    $query = "INSERT INTO `social`.`posts` (`userid`,`postmessage`, `postimg`, `postdoc`, `approved`) 
    VALUES ('$uid','$message', '$imgname', '$docname',$approved)";
    if($conn->execute($query) == TRUE) {
        $flag1 = move_uploaded_file($imgpath,$destimg);
        $flag2 = move_uploaded_file($docpath,$destdoc);
        if($flag1 and $flag2) {
            echo json_encode(array(
                "name"=>$data['name'],
                "status"=>$data['status'],
                "message"=>$message,
                "image"=>$imgname,
                "doc"=>$docname,
                "flag"=>true,
            ));
        } else {
            echo json_encode(array(
                "name" => $data['name'],
                "status" => $data['status'],
                "message" => $message,
                "image" => $imgname,
                "doc" => $docname,
                "flag" => true,
            ));
        }
    } else {
        echo json_encode(array(
            "flag" => $conn->error(),
        ));
    }
    $conn->close();
?>