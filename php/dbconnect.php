<?php

class Connect {
    private  $host = "localhost";
    private  $user = "root";
    private $pass = "chinna";
    private  $db = "social";
    private $conn;
    public function __construct()
    {
        $this->conn = new mysqli($this->host,$this->user,$this->pass,$this->db);
    }
    public function __toString()
    {
        if($this->conn->connect_error) {
            return "Connection Failed";
        } else {
            return "Successfully Connected";
        }
    }
    public function execute($query) {
        $result =$this->conn->query($query);
        return $result;
    }
    public function error() {
        return $this->conn->error;
    }
    public  function  close() {
        $this->conn->close();
    }
}

?>

