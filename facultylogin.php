<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/navbar.css">
</head>

<body>
    <header>
        <h2>COLLEGE &nbsp;SOCIALNETWORK</h2>
        <nav>
            <ul class="nav_links">
                <li><a href="studentlogin.php" >student</a></li>
                <li><a href="facultylogin.php" class="active">faculty</a></li>
                <li><a href="adminlogin.php">Admin</a></li>
            </ul>
        </nav>
        <a href="contactus.php" id="button">contact</a>
    </header>
    <div class="container" id="container">
        <div class="form-container sign-up-container">
            <form action="php/register.php" name="signup" method="POST" enctype="multipart/form-data" name="signup">
                <h1 style="font-size: 25px;">Create Account</h1>
                <span> </span>
                <input type="text" placeholder="FacultyId" name="uid" required />
                <input type="text" placeholder="Name" name="name" required/>
                <input type="email" placeholder="Email" name="mail" required/>
                <input type="date" name="dob" id="" placeholder="date of birth" required>
                <select name="gender"  required>
                    <option value="" selected disabled>Gender</option>
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                </select>
                <input type="password" placeholder="Password" name="pass" id="password" required />
                <input type="hidden" name="type" value="faculty">
                <button type="submit" value="submit" name="submit" id="submit"> Sign Up</button>
            </form>
        </div>
        <div class="form-container sign-in-container">
            <form action="#" method="POST" id="signup">
                <h1>Sign in</h1>
                <input type="email" placeholder="Email" name='mail' required/>
                <input type="password" placeholder="Password" name = 'pass' required/>
                <input type="hidden" name="type" value="faculty">
                <a href="#">Forgot your password?</a>
                <button type="submit" id="login">Sign In</button>
                <p id="error" style="text-align: center;"></p>
            </form>
        </div>
        <div class="overlay-container">
            <div class="overlay">
                <div class="overlay-panel overlay-left">
                    <h1>Welcome Back!</h1>
                    <p>To keep connected with us please login with your credentials</p>
                    <button class="ghost" id="signIn">Sign In</button>
                </div>
                <div class="overlay-panel overlay-right">
                    <h1>Hello, Friend!</h1>
                    <p>Enter your personal details and register</p>
                    <button class="ghost" id="signUp">Sign Up</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        const signUpButton = document.getElementById('signUp');
        const signInButton = document.getElementById('signIn');
        const container = document.getElementById('container');

        signUpButton.addEventListener('click', () => {
            container.classList.add("right-panel-active");
        });

        signInButton.addEventListener('click', () => {
            container.classList.remove("right-panel-active");
        });
    </script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="js/userlogin.js"></script>
</body>

</html>