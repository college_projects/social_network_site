<?php
session_start();
$baseid = $_SESSION['uid'];
include('php/dbconnect.php');
include('php/util.php');
$key = "";
$conn  = new Connect();
$request_query = "select  distinct * from social.userdata ud, social.friendrequests fr where ud.userid = fr.sender and fr.receiver = '$baseid' 
    and fr.sent=true and fr.friends=false;";
$friend_requests = $conn->execute($request_query);
if (isset($_POST['key'])) {
    $key = $_POST['key'];
    $query = "select  * from social.userdata as u where u.name like '%$key%' and u.approved = 'true' and u.userid != '$baseid' ;";
    $result = $conn->execute($query);
}
$navlink = "userprofile.php";
if($_SESSION['status'] == 'faculty') {
    $navlink = "facultyprofile.php";
}
$conn->close();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>b4-demo</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat+Alternates">
    <link rel="stylesheet" href="fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <style>
        #postcontents p {
            font-size: 19px;
        }
    </style>
</head>

<body style="font-family: 'Montserrat Alternates', sans-serif;background-color: rgb(255,255,255);">
    <nav class="navbar navbar-dark navbar-expand-md bg-dark fixed-top" style="max-width: 100%;font-size: 22px;">
        <div class="container"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#"><i class="fas fa-university"></i></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="<?php echo $navlink;?>">Home</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="friendrequests.php"><i class="fa fa-bell"></i></a>
                    </li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#"><i class="fa fa-cog"></i></a>
                    </li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="studentlogin.php"><i class="fa fa-power-off"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <br>
    <hr>
    <div class="row">
        <div class="col-md-7">
            <hr>
            <form action="#" method="post">
                <h1 style="margin:10px">Find People</h1>
                <div class="row" style="margin:10px">
                    <div class="col-md-10">
                        <div class="form-group">
                            <input type="text" name="key" id="key" class="form-control" value="<?php echo $key; ?>">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" name="search" id="search" class="btn btn-success btn-md btn-block">search</button>
                    </div>
                </div>
            </form>
            <div class="container overflow-auto" style="margin:10px;max-height:400px;">
                <ul class="list-group" id="reqlist" style="margin-left: 10px;margin-right: 10px;">
                    <?php
                    if (isset($_POST['key'])) {
                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                                $uid = $row['userid'];
                                $name = $row['name'];
                                $status = $row['status'];
                                $profimg = $row['pic'];
                                $profimg = "img/profiles/" . $profimg;
                                if (!isset($row['pic'])) {
                                    $profimg = "img/profile.jpg";
                                }
                                $name = PrintName($name, $status);
                                echo " <li class='list-group-item bg-light border border-dark' style='margin:5px;height:70px;' id='$uid'><span style='font-size: 18px;'><img class='border rounded-circle' src='$profimg' style='width: 48px;height: 48px;'>&nbsp; $name&nbsp;</span>
                                                <button type='button' name='sendrequest' id='$uid' class='btn btn-info' style='float: right;margin-top: 3px;'> send request</button></li>";
                            }
                        } else {
                            echo "<div class='alert alert-primary' role='alert'>
                                                <strong>No users are found </strong>
                                            </div>";
                        }
                    }
                    ?>
                </ul>
            </div>
            <hr>
        </div>
        <div class="col-md-5 border">
            <hr>
            <h1>Friend Requests</h1>
            <hr>
            <ul class="list-group overflow-auto" id="requests" style="margin-left: 10px;margin-right: 10px;max-height:500px">
                <?php
                if ($friend_requests->num_rows > 0) {
                    while ($row = $friend_requests->fetch_assoc()) {
                        $uid = $row['userid'];
                        $name = $row['name'];
                        $status = $row['status'];
                        $profimg = $row['pic'];
                        $profimg = "img/profiles/" . $profimg;
                        if (!isset($row['pic'])) {
                            $profimg = "img/profile.jpg";
                        }
                        $name = PrintName($name, $status);
                        echo " <li class='list-group-item bg-light border border-dark' style='margin:5px;height:70px;' id='$uid'><span style='font-size: 18px;'><img class='border rounded-circle' src='$profimg' style='width: 48px;height: 48px;'>&nbsp; $name&nbsp;</span>
                                        <button type='button' name='acceptrequest' id='$uid' class='btn btn-success' style='float: right;margin-top: 3px;'>Accept</button></li>";
                    }
                } else {
                    echo "<div class='alert alert-primary' role='alert'>
                                        <strong><center>No Friend Requests</center> </strong>
                                    </div>";
                }
                ?>
                
            </ul>
        </div>
    </div>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        $('[name=sendrequest]').click(function(e) {
            e.preventDefault();
            var receiver = $(this).attr('id');
            $.ajax({
                type: "post",
                url: "php/sendrequest.php",
                data: {
                    "receiver": receiver
                },
                dataType: "json",
                success: function(response) {
                    if (response.flag == true) {
                        $('#' + receiver).remove();
                    }
                }
            });
        });
        $('[name=acceptrequest]').click(function(e) {
            e.preventDefault();
            var sender = $(this).attr('id');
            $.ajax({
                type: "post",
                url: "php/acceptrequest.php",
                data: {
                    "sender": sender
                },
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    if (response.flag == true) {
                        $('#requests ' + '#' + sender).remove();
                    } else {
                        console.log(response);
                    }
                }
            });
        });
    </script>
</body>

</html>