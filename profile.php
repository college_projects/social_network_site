<?php
session_start();
include "php\dbconnect.php";
$uid = $_SESSION['uid'];
$conn = new Connect();
$query = "SELECT ud.name,ud.gmail,ud.number,ud.date,up.location,ud.pic FROM social.userdata as ud, social.user_profile as up where ud.userid = up.userid 
and up.userid = '$uid'";
$result = $conn->execute($query);
$assoc = $result->fetch_assoc();
$conn->close();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>b4-demo</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat+Alternates">
    <link rel="stylesheet" href="fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="css/Bootstrap-Chat.css">
    <link rel="stylesheet" href="css/project-card.css">
    <link rel="stylesheet" href="css/styles.css">
</head>

<body style="font-family: 'Montserrat Alternates', sans-serif;background-color: rgb(255,255,255);">
    <nav class="navbar navbar-dark navbar-expand-md bg-dark fixed-top" style="max-width: 100%;font-size: 22px;">
        <div class="container"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#"><i class="fas fa-university"></i></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link active" href=" #">Home</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#"><i class="fa fa-bell"></i></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#"><i class="fa fa-cog"></i></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#"><i class="fa fa-power-off"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="row" id="base" style="margin-top: 5%;margin-right: 0px;margin-left: 0px;">
        <div class="col-3 text-dark justify-content-center" style="padding-right: 10px;padding-left: 10px;padding-top: 10px;padding-bottom: 10px;">
            <div class="card style-border" style="width:270px;margin-left:30px;margin-right:15px">
                <img class="img-responsive" style="margin-top:10px;margin-left:10px;margin-right:10px" src="<?php if (isset($assoc['pic'])) {
                                                                                                                echo "img/profiles/" . $assoc['pic'];
                                                                                                            } else {
                                                                                                                echo "img/profile.jpg";
                                                                                                            } ?>" width="250px" height="250px" id="pimg">
                <div class="card-body">
                    <h5 class="card-title">profile</h5>
                    <p style="margin-top: 5px;margin-bottom: 5px;"><i class="far fa-user"></i><b id="pname"><?php echo $assoc['name']; ?></b></p>
                    <p style="margin-top: 5px;margin-bottom: 5px;"><i class="fa fa-calendar"></i>;<b id="pdob"><?php echo $assoc['date']; ?></b></p>
                    <p style="margin-top: 5px;margin-bottom: 5px;font-size:10px"><i class="icon ion-email"></i><b id="pmail"><?php echo $assoc['gmail']; ?></b></p>
                    <p style="margin-top: 5px;margin-bottom: 5px;"><i class="fa fa-mobile-phone"></i><b id="pnum"><?php if (isset($assoc['number'])) {
                                                                                                                        echo $assoc['number'];
                                                                                                                    } else {
                                                                                                                        echo "please update";
                                                                                                                    } ?></b></p>
                    <p style="margin-top: 5px;margin-bottom: 5px;"><i class="fa fa-location-arrow"></i><b id="ploc"><?php if (isset($assoc['location'])) {
                                                                                                                        echo $assoc['location'];
                                                                                                                    } else {
                                                                                                                        echo "please update";
                                                                                                                    } ?></b></p>
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modelId">update&nbsp;</button>
                </div>
            </div>
        </div>
        <div class="col-6 text-dark justify-content-center overflow-auto" id="divcol" style="font-size: 30px;padding-top: 2px;padding-right: 8px;max-height: 530px;">
            <div class="container style-border" style="background-color: rgb(248,248,252);padding-right: 0px;padding-bottom: 0px;padding-left: 0px;margin-bottom: 20px;">
                <h6 style="height: 31px;font-size: 29px;margin-top: 4px;">want to post someting</h6>
                <form><textarea class="form-control" style="height: 62px;padding: 6px;padding-right: 5px;padding-left: 0px;margin-right: 10px;margin-left: 10px;max-width: 100%;width: auto;min-width: 96%;"></textarea>
                    <div class="form-row" style="height: 53px;padding-top: 8px;margin-right: 8px;margin-left: 8px;">
                        <div class="col" style="height: 44px;"><input type="file" class="far fa-file-image" accept="image/*" style="width: 23px;height: 30px;padding-top: 3px;padding-right: 24px;"></div>
                        <div class="col" style="height: 44px;"><input type="file" class="fas fa-file-upload" style="width: 23px;height: 30px;"></div>
                        <div class="col" style="height: 44px;"><button class="btn btn-primary float-right" type="submit">Post</button></div>
                    </div>
                </form>
            </div>
            <div class="container style-border" id="post" style="margin-top: 5px;margin-bottom: 5px;">
                <div class="row" style="max-height: 50px;background-color: #222121;color: rgb(236,240,244);">
                    <div class="col-auto align-self-start" style="width: 51px;"><img class="rounded-circle" src="img/profile.jpg" style="min-height: 38px;width: 45px;height: 45px;padding-top: 2px;padding-right: 1px;padding-bottom: 5px;padding-left: 3px;"></div>
                    <div class="col-8" style="height: 67px;font-size: 27px;">
                        <p style="width: 11px;height: 10px;margin-top: 13px;margin-bottom: 10px;">username</p>
                    </div>
                </div>
                <div class="row" style="padding-top: 5px;padding-right: 5px;padding-bottom: 5px;padding-left: 5px;margin-top: 5px;margin-right: 5px;margin-bottom: 5px;margin-left: 5px;height: auto;max-height: 100%;"><img src="img/meeting.jpg" style="max-height: 100%;height: auto;max-width: 100%;margin-top: 5px;margin-right: 5px;margin-bottom: 5px;margin-left: 5px;">
                    <p class="message-border" style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;margin-right: 0px;">&nbsp;message by user :&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>
                    <p class="message-border" style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;margin-right: 0px;"><i class="fa fa-file-pdf-o" style="font-size: 23px;margin-top: 3px;margin-right: 3px;margin-bottom: 3px;margin-left: 3px;"></i><i class="fa fa-file-excel-o" style="font-size: 23px;"></i><i class="fa fa-file-word-o" style="font-size: 23px;"></i>&nbsp;
                        document&nbsp;name&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                </div>
            </div>
            <div class="container style-border" id="post" style="margin-top: 5px;margin-bottom: 5px;">
                <div class="row" style="max-height: 50px;background-color: #222121;color: rgb(236,240,244);">
                    <div class="col-auto align-self-start" style="width: 51px;"><img class="rounded-circle" src="img/meeting.jpg" style="min-height: 38px;width: 45px;height: 45px;padding-top: 2px;padding-right: 1px;padding-bottom: 5px;padding-left: 3px;"></div>
                    <div class="col" style="height: 67px;font-size: 27px;">
                        <p style="width: 11px;height: 10px;margin-top: 13px;margin-bottom: 10px;">username</p>
                    </div>
                </div>
                <div class="row" style="padding-top: 5px;padding-right: 5px;padding-bottom: 5px;padding-left: 5px;margin-top: 5px;margin-right: 5px;margin-bottom: 5px;margin-left: 5px;height: auto;max-height: 100%;"><img src="img/meeting.jpg" style="max-height: 100%;height: auto;max-width: 100%;margin-top: 5px;margin-right: 5px;margin-bottom: 5px;margin-left: 5px;">
                    <p class="message-border" style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;margin-right: 0px;">&nbsp;message by user :&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>
                    <p class="message-border" style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;margin-right: 0px;"><i class="fa fa-file-pdf-o" style="font-size: 23px;margin-top: 3px;margin-right: 3px;margin-bottom: 3px;margin-left: 3px;"></i><i class="fa fa-file-excel-o" style="font-size: 23px;"></i><i class="fa fa-file-word-o" style="font-size: 23px;"></i>&nbsp;
                        document&nbsp;name&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                </div>
            </div>
        </div>
        <div class="col-3 text-dark justify-content-center overflow-auto" style="padding-right: 0px;padding-left: 0px;margin-top: 5px;margin-bottom: 5px;max-height: 530px;">
            <ul class="list-group" style="margin-left: 10px;margin-right: 10px;">
                <li class="list-group-item" style="background-color: rgb(7,7,7);margin-right: 0px;"><span style="font-size: 30px;color: rgb(255,255,255);background-color: #000000;">Friends List</span></li>
                <li class="list-group-item"><span style="font-size: 21px;"><img class="border rounded-circle" src="img/meeting.jpg" style="width: 48px;height: 48px;">&nbsp; username&nbsp;</span></li>
                <li class="list-group-item"><span style="font-size: 21px;"><img class="border rounded-circle" src="img/meeting.jpg" style="width: 48px;height: 48px;">&nbsp; username&nbsp;</span></li>
                <li class="list-group-item"><span style="font-size: 21px;"><img class="border rounded-circle" src="img/meeting.jpg" style="width: 48px;height: 48px;">&nbsp; username&nbsp;</span></li>
                <li class="list-group-item"><span style="font-size: 21px;"><img class="border rounded-circle" src="img/meeting.jpg" style="width: 48px;height: 48px;">&nbsp; username&nbsp;</span></li>
                <li class="list-group-item"><span style="font-size: 21px;"><img class="border rounded-circle" src="img/meeting.jpg" style="width: 48px;height: 48px;">&nbsp; username&nbsp;</span></li>
                <li class="list-group-item"><span style="font-size: 21px;"><img class="border rounded-circle" src="img/meeting.jpg" style="width: 48px;height: 48px;">&nbsp; username&nbsp;</span></li>
                <li class="list-group-item"><span style="font-size: 21px;"><img class="border rounded-circle" src="img/meeting.jpg" style="width: 48px;height: 48px;">&nbsp; username&nbsp;</span></li>
            </ul>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="update_profile" method="POST" action="#">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Username" name="name" required>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" placeholder="Email Address" name="mail" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Mobile Number" name="number" required>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Location" name="location" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Date of Birth</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="date" class="form-control" aria-describedby="helpId" placeholder="" name="date" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Primary Education <i class="fa fa-percent"></i>/CGPA</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" aria-describedby="helpId" placeholder="" name="ssc" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Secondary Education <i class="fa fa-percent"></i>/CGPA</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="" aria-describedby="helpId" placeholder="" name="inter" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Current Degree <i class="fa fa-percent"></i>/CGPA</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" name="btech" id="" aria-describedby="helpId" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Backlogs</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" name="backlogs" id="" aria-describedby="helpId" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Upload your image</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="file" class="form-control-file" name="file" id="image" accept="image/*" placeholder="" required>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md" id="error" style="text-align: center;font-size:12px">
                        </div>
                        <div class="col-md"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div>
                        <div class="col-md"><button type="submit" class="btn btn-primary" id="update">Save</button></div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/profileupdate.js"></script>
</body>

</html>