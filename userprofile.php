<?php
session_start();
include "php\dbconnect.php";
$uid = $_SESSION['uid'];
$conn = new Connect();
$query = "SELECT * FROM social.userdata as ud, social.user_profile as up where ud.userid = up.userid 
and up.userid = '$uid' and ud.status = 'student'";
$result = $conn->execute($query);
$assoc = $result->fetch_assoc();
$query = "select * from social.userdata as ud, social.posts as p where p.userid = ud.userid
and p.approved = true order by  p.time desc ;";
$postdata = $conn->execute($query);
function PrintName($name, $status)
{
    if ($status == "admin") {
        $name = "Administrator";
        return $name;
    } else {
        $name =  $name . "(" . $status . ")";
        return $name;
    }
}
$request_query = "select  distinct * from social.userdata ud, social.friendrequests fr where ud.userid = fr.sender and fr.receiver = '$uid' 
    and fr.sent=true and fr.friends=true;";
$friend_requests = $conn->execute($request_query);
$conn->close();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>b4-demo</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat+Alternates">
    <link rel="stylesheet" href="fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <style>
        #postcontents p {
            font-size: 19px;
        }
    </style>
</head>

<body style="font-family: 'Montserrat Alternates', sans-serif;background-color: rgb(255,255,255);">
    <nav class="navbar navbar-dark navbar-expand-md bg-dark fixed-top" style="max-width: 100%;font-size: 22px;">
        <div class="container"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#"><i class="fas fa-university"></i></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="userprofile.php">Home</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="friendrequests.php"><i class="fa fa-bell"></i></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#"><i class="fa fa-cog"></i></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="studentlogin.php"><i class="fa fa-power-off"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="row" id="base" style="margin-top: 5%;margin-right: 0px;margin-left: 0px;">
        <div class="col-3 text-dark justify-content-center" style="padding-right: 10px;padding-left: 10px;padding-top: 10px;padding-bottom: 10px;">
            <div class="card style-border" style="width:270px;margin-left:30px;margin-right:15px">
                <img class="img-responsive" style="margin-top:10px;margin-left:10px;margin-right:10px" src="<?php if (isset($assoc['pic'])) {
                                                                                                                echo "img/profiles/" . $assoc['pic'];
                                                                                                            } else {
                                                                                                                echo "img/profile.jpg";
                                                                                                            } ?>" width="250px" height="250px" id="pimg">
                <div class="card-body">
                    <h5 class="card-title">profile</h5>
                    <p style="margin-top: 5px;margin-bottom: 5px;"><i class="far fa-user"></i><b id="pname"><?php echo $assoc['name']; ?></b></p>
                    <p style="margin-top: 5px;margin-bottom: 5px;"><i class="fa fa-calendar"></i>;<b id="pdob"><?php echo $assoc['date']; ?></b></p>
                    <p style="margin-top: 5px;margin-bottom: 5px;font-size:10px"><i class="icon ion-email"></i><b id="pmail"><?php echo $assoc['gmail']; ?></b></p>
                    <p style="margin-top: 5px;margin-bottom: 5px;"><i class="fa fa-mobile-phone"></i><b id="pnum"><?php if (isset($assoc['number'])) {
                                                                                                                        echo $assoc['number'];
                                                                                                                    } else {
                                                                                                                        echo "please update";
                                                                                                                    } ?></b></p>
                    <p style="margin-top: 5px;margin-bottom: 5px;"><i class="fa fa-location-arrow"></i><b id="ploc"><?php if (isset($assoc['location'])) {
                                                                                                                        echo $assoc['location'];
                                                                                                                    } else {
                                                                                                                        echo "please update";
                                                                                                                    } ?></b></p>
                    <button class="btn btn-primary" type="button" id="update">update&nbsp;</button>
                </div>
            </div>
        </div>
        <div class="col-6 text-dark justify-content-center overflow-auto" id="divcol" style="font-size: 30px;padding-top: 2px;padding-right: 8px;max-height: 530px;">
            <div class="container style-border" style="background-color: rgb(248,248,252);padding-right: 0px;padding-bottom: 0px;padding-left: 0px;margin-bottom: 20px;">
                <h6 style="height: 31px;font-size: 29px;margin-top: 4px;">want to post someting</h6>
                <form action="#" method="post" id="postform"><textarea class="form-control" style="height: 62px;padding: 6px;padding-right: 5px;padding-left: 0px;margin-right: 10px;margin-left: 10px;max-width: 100%;width: auto;min-width: 96%;" id="postmessage" name="postmessage" required></textarea>
                    <div class="form-row" style="height: 53px;padding-top: 8px;margin-right: 8px;margin-left: 8px;">
                        <div class="col" style="height: 44px;"><input type="file" class="far fa-file-image" accept="image/*" style="width: 23px;height: 30px;padding-top: 3px;padding-right: 24px;" id="image" name="image"></div>
                        <div class="col" style="height: 44px;"><input type="file" class="fas fa-file-upload" style="width: 23px;height: 30px;" id="doc" name="doc"></div>
                        <div class="col" style="height: 44px;"><button class="btn btn-primary float-right" type="submit">Post</button></div>
                    </div>
                </form>
            </div>
            <div id="postcontents">
                <?php
                while ($row = $postdata->fetch_assoc()) {
                    $profimg = $row['pic'];
                    $name = $row['name'];
                    $status = $row['status'];
                    $psmg = $row['postmessage'];
                    $pimage = $row['postimg'];
                    $pdoc = $row['postdoc'];
                    $postid = $row['postid'];
                    #####write for extensions
                    $profimg = "img/profiles/" . $profimg;
                    if (!isset($row['pic'])) {
                        $profimg = "img/profile.jpg";
                    }
                ?>
                    <div class="container style-border" id="post" style="margin-top: 1px;margin-bottom: 10px;">
                        <div class="row" style="max-height: 50px;background-color: #222121;color: rgb(236,240,244);">
                            <div class="col-auto align-self-start" style="width: 51px;"><img class="rounded-circle" src="<?php echo $profimg; ?>" style="min-height: 38px;width: 45px;height: 45px;padding-top: 2px;padding-right: 1px;padding-bottom: 5px;padding-left: 3px;"></div>
                            <div class="col-8" style="height: 67px;font-size: 27px;">
                                <p style="width: auto;height: 10px;margin-top: 13px;margin-bottom: 10px;"><?php echo PrintName($name, $status); ?></p>
                            </div>
                        </div>
                        <div class="container" style="margin:5px;height: auto;max-height: 100%;padding:4px">
                            <?php if ($pimage != "") { ?>
                                <center>
                                    <img src="<?php echo "img/posts/" . $pimage; ?>" style="max-height: 100%;height: auto;max-width: 100%;height:400px">
                                </center>
                            <?php  } ?>
                            <hr>
                            <p style="margin-bottom:10px;font-size:16px"><?php echo $psmg; ?></p>
                            <hr>
                            <?php if ($pdoc != "") { ?>
                                <p style="margin-bottom:10px;font-size:16px"><a href="docs/<?php echo $pdoc; ?>"><?php echo $pdoc; ?></a></p>
                                <hr>
                            <?php } ?>

                        </div>
                        <div class="form-group row bg-dark" id="<?php echo "post" . $postid ?>">

                            <div class="col-md-8">
                                <input type="text" class="form-control" id="comment" name="inputName" id="inputName" style="margin-top:4px;" placeholder="" required>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" name='<?php echo $postid ?>' id="makecomment" class="btn btn-primary" style="margin-bottom:5px;">comment</button>
                            </div>
                            <div class="col-md-2">
                                <center><button class="fa fa-search" data-toggle="modal" data-target="<?php echo '#modelId' . $postid; ?>"></button></center>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="<?php echo 'modelId' . $postid; ?>" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                            <div class="modal-dialog modal-md" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title">comments</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?php
                                        $connection = new Connect();
                                        $comments_query = "select * from social.userdata as ud, social.comments as cmt  where cmt.userid = ud.userid and cmt.postid = $postid order by cmt.time;";
                                        $comments_result = $connection->execute($comments_query);
                                        $connection->close();
                                        ?>
                                        <div class="container-fluid overflow-auto" style="max-height: 400px;">
                                            <div class="accordion" id="accordionExample">
                                                <?php
                                                if ($comments_result->num_rows > 0) {
                                                    while ($row = $comments_result->fetch_assoc()) {
                                                        $name = $row['name'];
                                                        $status = $row['status'];
                                                        $name = PrintName($name, $status);
                                                        $profimg = $row['pic'];
                                                        $profimg = "img/profiles/" . $profimg;
                                                        if (!isset($row['pic'])) {
                                                            $profimg = "img/profile.jpg";
                                                        }
                                                        $comment = $row['comment'];
                                                        $cmtid = $row['commentid'];
                                                ?>
                                                        <div class="card">
                                                            <div class="card-header" id="headingOne">
                                                                <h2 class="mb-0">
                                                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="<?php echo '#collapseOne' . $cmtid; ?>" aria-expanded="true" aria-controls="collapseOne">
                                                                        <img class="rounded-circle" src="<?php echo $profimg; ?>" style="min-height: 38px;width: 45px;height: 45px;padding-top: 2px;padding-right: 1px;padding-bottom: 5px;padding-left: 3px;">
                                                                        <span style="font-size:18px;font-weight:bold;"><?php echo $name; ?></span>
                                                                    </button>
                                                                </h2>
                                                            </div>
                                                            <div id="<?php echo 'collapseOne' . $cmtid; ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                                <div class="card-body" style="font-size: 16px;">
                                                                    <?php echo $comment ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php  }
                                                } else { ?>
                                                    <div class="alert alert-primary" role="alert">
                                                        <center><strong style="font-size:16px">No comments on this post</strong></center>
                                                    </div>
                                                <?php  } ?>

                                            </div>


                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-3 text-dark justify-content-center overflow-auto" style="padding-right: 0px;padding-left: 0px;margin-top: 5px;margin-bottom: 5px;max-height: 530px;">
            <ul class="list-group" style="margin-left: 10px;margin-right: 10px;">
                <li class="list-group-item" style="background-color: rgb(7,7,7);margin-right: 0px;"><span style="font-size: 30px;color: rgb(255,255,255);background-color: #000000;">Friends List</span></li>
                <?php
                if ($friend_requests->num_rows > 0) {
                    while ($row = $friend_requests->fetch_assoc()) {
                        $uid = $row['userid'];
                        $name = $row['name'];
                        $status = $row['status'];
                        $profimg = $row['pic'];
                        $profimg = "img/profiles/" . $profimg;
                        if (!isset($row['pic'])) {
                            $profimg = "img/profile.jpg";
                        }
                        $name = PrintName($name, $status);
                        echo " <li class='list-group-item bg-light border border-dark' style='margin:3px;height:70px;' id='$uid'><span style='font-size: 15px;font-weight: bold;'><img class='border rounded-circle' src='$profimg' style='width: 48px;height: 48px;'>$name</span></li>";
                    }
                } else {
                    echo "<div class='alert alert-primary' role='alert'>
                                        <center><strong>No Friends </strong></center>
                                    </div>";
                }
                ?>
            </ul>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="update_profile" method="POST" action="#">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Username" name="name" id="name" required>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" placeholder="Email Address" name="mail" id="mail" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Mobile Number" name="number" id="number" required>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Location" name="location" id="location" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Date of Birth</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="date" class="form-control" aria-describedby="helpId" placeholder="" id="date" name="date" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Primary Education <i class="fa fa-percent"></i>/CGPA</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" aria-describedby="helpId" id="ssc" name="ssc" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Secondary Education <i class="fa fa-percent"></i>/CGPA</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="" aria-describedby="helpId" placeholder="" name="inter" id="inter" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Current Degree <i class="fa fa-percent"></i>/CGPA</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" name="btech" id="btech" aria-describedby="helpId" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Backlogs</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" name="backlogs" id="backlogs" aria-describedby="helpId" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Upload your image</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="file" class="form-control-file" name="file" id="image" accept="image/*" placeholder="" required>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md" id="error" style="text-align: center;font-size:12px">
                        </div>
                        <div class="col-md"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div>
                        <div class="col-md"><button type="submit" class="btn btn-primary" id="update">Save</button></div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/profileupdate.js"></script>
    <script src="js/insertpost.js"></script>
    <script>
        $("#update").click(function(e) {
            e.preventDefault();
            var name = $("#pname").text();
            var mail = $("#pmail").text();
            var dob = $("#pdob").text();
            var location = $("#ploc").text();
            var mobile = $("#pnum").text();
            var img = $("#pimg").attr('src');
            var btech = "<?php if (isset($assoc['current_aggregate'])) {
                                echo $assoc['current_aggregate'];
                            } ?>";
            var ssc = "<?php if (isset($assoc['schooling_grade'])) {
                            echo $assoc['schooling_grade'];
                        } ?>";
            var backlogs = "<?php if (isset($assoc['backlogs'])) {
                                echo $assoc['backlogs'];
                            } ?>";
            var inter = "<?php if (isset($assoc['secondary_grade'])) {
                                echo $assoc['secondary_grade'];
                            } ?>";
            console.log("inter" + inter);
            $("#name").val(name);
            $("#mail").val(mail);
            $("#date").val(dob);
            $("#location").val(location);
            $("#number").val(mobile);
            $("#image").attr("value", img);
            $("#ssc").val(ssc);
            $("[name=inter]").val(inter);
            $("#btech").val(btech);
            $("#backlogs").val(backlogs);
            $("#modelId").modal('show');
        });
        $("#makecomment").click(function(e) {
            e.preventDefault();
            var postid = $(this).attr('name');
            var selector = "#post" + postid;
            var comment = $(selector + " #comment").val();
            if (comment != "") {
                $.ajax({
                    type: "post",
                    url: "php/addcomment.php",
                    data: {
                        "postid": postid,
                        "comment": comment
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.flag == true) {
                            location.reload(true);
                        }
                    }
                });
            } else {
                alert('you cant post empty comments');
            }
        });
    </script>
</body>

</html>