<?php
session_start();
include "php\dbconnect.php";
$conn = new Connect();
$query = "select ud.name,ud.pic,ud.status,p.postmessage,p.postdoc,p.postimg from social.userdata as ud, social.posts as p where p.userid = ud.userid
and p.approved = true order by  p.time desc ;";
$postdata = $conn->execute($query);
function PrintName($name, $status)
{
    if ($status == "admin") {
        $name = "Administrator";
        return $name;
    } else {
        $name =  $name . "(" . $status . ")";
        return $name;
    }
}
$conn->close();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>b4-demo</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat+Alternates">
    <link rel="stylesheet" href="fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <style>
        body {
            overflow-y: auto;
        }

        #postcontents p {
            font-size: 19px;
        }
    </style>
</head>

<body style="background-color: rgb(255,255,255);">
    <nav class="navbar navbar-dark navbar-expand-md bg-dark" style="max-width: 100%;">
        <div class="container"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#"><i class="fas fa-university"></i></a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Student</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="admin.student.approval.php">Approval</a>
                            <a class="dropdown-item" href="admin.student.list.php">list</a>
                            <a class="dropdown-item" href="admin.post.approval.php">Post Approval</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Faculty</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="admin.faculty.approval.php">Approval</a>
                            <a class="dropdown-item" href="admin.faculty.list.php">List</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Post</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item active" href="admin.post.php">Information</a>
                            <a class="dropdown-item" href="#">Student List</a>
                        </div>
                    </li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#"><i class="fa fa-power-off"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <hr>
    <div class="container justify-content-center" id="divcol" style="font-size: 30px;padding-top: 2px;padding-right: 8px;max-height: 530px;">
        <div class="container style-border" style="background-color: rgb(248,248,252);padding-right: 0px;padding-bottom: 0px;padding-left: 0px;margin-bottom: 20px;">
            <h6 style="height: 31px;font-size: 29px;margin-top: 4px;">&nbsp;&nbsp;&nbsp;Add Post</h6>
            <form action="#" method="post" id="postform"><textarea class="form-control" style="height: 62px;padding: 6px;padding-right: 5px;padding-left: 0px;margin-right: 10px;margin-left: 10px;max-width: 100%;width: auto;min-width: 96%;" id="postmessage" name="postmessage" required></textarea>
                <div class="form-row" style="height: 53px;padding-top: 8px;margin-right: 8px;margin-left: 8px;">
                    <div class="col" style="height: 44px;"><input type="file" class="far fa-file-image" accept="image/*" style="width: 23px;height: 30px;padding-top: 3px;padding-right: 24px;" id="image" name="image"></div>
                    <div class="col" style="height: 44px;"><input type="file" class="fas fa-file-upload" style="width: 23px;height: 30px;" id="doc" name="doc"></div>
                    <div class="col" style="height: 44px;"><button class="btn btn-primary float-right" type="submit">Post</button></div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-2">
            </div>
            <div id="postcontents" class="col-md-8">
                <?php
                while ($row = $postdata->fetch_assoc()) {
                    $profimg = $row['pic'];
                    $name = $row['name'];
                    $status = $row['status'];
                    $psmg = $row['postmessage'];
                    $pimage = $row['postimg'];
                    $pdoc = $row['postdoc'];
                    #####write for extensions
                    $profimg = "img/profiles/" . $profimg;
                    if(!isset($row['pic'])) {
                        $profimg = "img/profile.jpg";
                    }
                ?>
                    <div class="container style-border" id="post" style="margin-top: 1px;margin-bottom: 10px;">
                        <div class="row" style="max-height: 50px;background-color: #222121;color: rgb(236,240,244);">
                            <div class="col-auto align-self-start" style="width: 51px;"><img class="rounded-circle" src="<?php echo $profimg; ?>" style="min-height: 38px;width: 45px;height: 45px;padding-top: 2px;padding-right: 1px;padding-bottom: 5px;padding-left: 3px;"></div>
                            <div class="col-8" style="height: 67px;font-size: 27px;">
                                <p style="width: auto;height: 10px;margin-top: 13px;margin-bottom: 10px;"><?php echo PrintName($name,$status)  ?></p>
                            </div>
                        </div>
                        <div class="container" id="postbody" style="margin:5px;height: auto;max-height: 100%;padding:4px">
                            <?php if ($pimage != "") { ?>
                                <center>
                                    <img src="<?php echo "img/posts/" . $pimage; ?>" style="max-height: 100%;height: auto;max-width: 100%;height:400px">
                                </center>
                            <?php  } ?>
                            <hr>
                            <p style="margin-bottom:10px;font-size:17px"><?php echo $psmg; ?></p>
                            <hr>
                            <?php if ($pdoc != "") { ?>
                                <p style="margin-bottom:10px;font-size:17px"><a href="docs/<?php echo $pdoc; ?>"><?php echo $pdoc; ?></a></p>
                                <hr>
                            <?php } ?>

                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/insertpost.js"></script>
</body>

</html>