<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/navbar.css">
    <style>
      body {
        background-color: lightslategrey;
        font-family: 'Montserrat', sans-serif;
      }
      .form-container {
        background-color: whitesmoke;
        padding: 40px;
        border-radius: 10px;
        box-shadow: 0px 0px 5px 5px #000;
        margin-top: 40px;
      }
    </style>
</head>
  <body>
    <header>
        <h2>COLLEGE &nbsp;SOCIALNETWORK</h2>
        <nav>
            <ul class="nav_links">
                <li><a href="studentlogin.php">student</a></li>
                <li><a href="facultylogin.php" >faculty</a></li>
                <li><a href="adminlogin.php" class="active">Admin</a></li>
            </ul>
        </nav>
        <a href="contactus.php" id="button">contact</a>
    </header>
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-13 col-sm-6 col-md-4">
            <div class="form-container">
                <form action="#" id="signup">
                  <h3 align="center" style="font-weight: 600;">Admin Login</h3>
                  <div class="form-group">
                    <input type="email" class="form-control form-control-lg" id="mail" placeholder="Email address"
                      aria-describedby="emailHelp" name="mail" required>
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control form-control-lg" id="pass" placeholder="Password" name="pass" required>
                  </div>
                  <input type="hidden" name="type" value="admin">
                  <button type="submit" class="btn btn-primary btn-md btn-block" id="login">Submit</button>
                  <p> </p>
                  <p id="error" style="text-align: center;"></p>
                </form>
            </div>
            
          </div>
      </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/userlogin.js"></script>
  </body>
</html>