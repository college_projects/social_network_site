<?php
include('php/dbconnect.php');
$conn = new Connect();
$type = 'faculty';
$query = "SELECT t.userid, t.name, t.gmail, FROM social.userdata t where t.status = '$type' and t.approved = 'false'";
$result = $conn->execute($query);
?>
<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>pending-approvals</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat+Alternates">
    <link rel="stylesheet" href="fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css" />
</head>
</head>

<body>
    <div>

    </div>
    <nav class="navbar navbar-dark navbar-expand-md bg-dark" style="max-width: 100%;">
        <div class="container"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#"><i class="fas fa-university"></i></a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Student</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item " href="admin.student.approval.php">Approval</a>
                            <a class="dropdown-item" href="#">list</a>
                            <a class="dropdown-item" href="#">Post Approval</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Faculty</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item active" href="admin.faculty.approval.php">Approval</a>
                            <a class="dropdown-item" href="#">List</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Post</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="admin.post.php">Information</a>
                            <a class="dropdown-item" href="#">Student List</a>
                        </div>
                    </li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#"><i class="fa fa-power-off"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <br>
    <br>
    <div class="container">
        <table class="table table-info table-striped" id="Mytable">
            <thead class="thead thead-dark">
                <tr>
                    <th scope="col">FacultyId</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Approve</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while ($row = $result->fetch_assoc()) {
                    $uid = $row['userid'];
                    $name = $row['name'];
                    $mail = $row['gmail'];
                    echo "<tr id='$uid'>
                                <th scope='row'>$uid</th>
                                <td>$name</td>
                                <td>$mail</td>
                                <td><center><button class='btn btn-success' id='$uid' name='delete'>Approve</button></center></td>
                              </tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
    <script>
        $("#Mytable").DataTable({
            dom: 'lBfrtip',
            buttons: [
                'excel', 'csv', 'copy'
            ],
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ]
        });
        $(document).on('click', '[name=delete]', function() {
            var id = $('[name=delete]').attr('id');
            $.ajax({
                type: "POST",
                url: "php/userapprove.php",
                data: {
                    'uid': id,
                    'status': 'faculty',
                },
                dataType: "JSON",
                success: function(response) {
                    if (response.flag == true) {
                        var id = '#' + $('[name=delete]').attr('id');
                        $(id).remove();
                        location.reload(true);
                    }
                }
            });

        });
    </script>
</body>

</html>