<?php
include('php/dbconnect.php');
$conn = new Connect();
$type = 'faculty';
$query = "select * from social.userdata as ud , social.facultydata as fd where ud.userid = fd.userid and ud.approved = 'true'";
$result = $conn->execute($query);
?>
<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>pending-approvals</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat+Alternates">
    <link rel="stylesheet" href="fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css" />
    <style>
        body {
            overflow: auto;
        }
    </style>
</head>

<body>
    <div>

    </div>
    <nav class="navbar navbar-dark navbar-expand-md bg-dark" style="max-width: 100%;">
        <div class="container"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#"><i class="fas fa-university"></i></a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Student</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item " href="admin.student.approval.php">Approval</a>
                            <a class="dropdown-item" href="admin.student.list.php">list</a>
                            <a class="dropdown-item" href="admin.post.approval.php">Post Approval</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Faculty</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="admin.faculty.approval.php">Approval</a>
                            <a class="dropdown-item active" href="admin.faculty.list.php">List</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Post</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="admin.post.php">Information</a>
                            <a class="dropdown-item" href="#">Student List</a>
                        </div>
                    </li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#"><i class="fa fa-power-off"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <br>
    <div class="container">
        <div class="text-right mb-3">
            <button type="button" name="modalbutton" id="modalbutton" class="btn btn-primary">Add Faculty</button>
        </div>
        <hr>
        <table class="table table-info table-striped" id="Mytable">
            <thead class="thead thead-dark">
                <tr>
                    <th scope="col">FacultyId</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Department</th>
                    <th scope="col">Designation</th>
                    <th scope="col">Expertise</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while ($row = $result->fetch_assoc()) {
                    $uid = $row['userid'];
                    $name = $row['name'];
                    $mail = $row['gmail'];
                    $dob = $row['date'];
                    $gender = $row['gender'];
                    $location = $row['location'];
                    $number = $row['number'];
                    $dept = $row['department'];
                    $expt = $row['expertise'];
                    $designation = $row['designation'];
                    $pid = 'p' . $uid;
                    echo "<tr id='$pid' name='$pid'>
                                <th scope='row' id='tid'>$uid</th>
                                <td id='tname'>$name</td>
                                <td id='tmail'>$mail</td>
                                <td id='tdept'>$dept</td>
                                <td id='tdesc'>$designation</td>
                                <td id='texpt'>$expt</td>
                                <input type='hidden' id='tdob' value='$dob'>
                                <input type='hidden' id='tgender' value='$gender'>
                                <input type='hidden' id='tlocation' value='$location'>
                                <input type='hidden' id='tnumber' value='$number'>
                                <td><center><button class='btn btn-warning' id='$uid' name='update'>Update</button> <button class='btn btn-danger' id='$uid' name='delete'>Delete</button></center></td>
                              </tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Faculty</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="insert_faculty" method="POST" action="#">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="FacultyID" name="facid" required>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="FullName" name="name" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="mail" class="form-control" placeholder="Email" name="mail" required>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Contact Number" name="number" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Location" name="place" required>
                            </div>
                            <div class="form-group col-md-6">
                                <select class="form-control" id="sel1" name="gender">
                                    <option value='' disabled selected>Gender</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Date of Birth</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="date" class="form-control" aria-describedby="helpId" placeholder="" name="date" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Department</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" aria-describedby="helpId" placeholder="" name="dept" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Designation</label>
                            </div>

                            <div class="form-group col-md-6">
                                <select class="form-control" id="sel1" name="designation">
                                    <option value="Professor">Professor</option>
                                    <option value="Assistant Professor">Assistant Professor</option>
                                    <option value="Associate Professor">Associate Professor</option>
                                    <option value="HOD">HOD</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Expertise</label>
                            </div>

                            <div class="form-group col-md-6">
                                <textarea class="form-control" name="expt" id="expt" rows="2" required></textarea>

                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md" id="error" style="text-align: center;font-size:12px">
                        </div>
                        <div class="col-md"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div>
                        <div class="col-md"><button type="submit" class="btn btn-primary" id="update">Save</button></div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--user update model -->
    <div class="modal fade" id="updatefaculty" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Faculty</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="update_faculty" method="POST" action="#">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="FacultyID" name="facid" id="facid" required>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="FullName" name="name" id="name" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="mail" class="form-control" placeholder="Email" name="mail" id="mail" required>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Contact Number" name="number" id="number" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Location" name="place" id="place" required>
                            </div>
                            <div class="form-group col-md-6">
                                <select class="form-control" id="gender" name="gender">
                                    <option value='' disabled selected>Gender</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Date of Birth</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="date" class="form-control" aria-describedby="helpId" placeholder="" name="date" id="date" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Department</label>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" aria-describedby="helpId" placeholder="" name="dept" id="dept" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Designation</label>
                            </div>

                            <div class="form-group col-md-6">
                                <select class="form-control" id="designation" name="designation">
                                    <option value="Professor">Professor</option>
                                    <option value="Assistant Professor">Assistant Professor</option>
                                    <option value="Associate Professor">Associate Professor</option>
                                    <option value="HOD">HOD</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label style="font-size: 14px;font-weight: bold;margin-top: 5px;">Expertise</label>
                            </div>

                            <div class="form-group col-md-6">
                                <textarea class="form-control" name="expt" id="expt" rows="2" required></textarea>

                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md" id="error" style="text-align: center;font-size:12px">
                        </div>
                        <div class="col-md"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div>
                        <div class="col-md"><button type="submit" class="btn btn-primary" id="update">Save</button></div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script>
        var table = $("#Mytable").DataTable({
            dom: 'lBfrtip',
            buttons: [
                'excel', 'csv', 'copy'
            ],
            "lengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ]
        });
        $('[name=delete]').click(function(e) {
            var id = $(this).attr('id');

            $.ajax({
                type: "POST",
                url: "php/userdelete.php",
                data: {
                    'uid': id,
                },
                dataType: "JSON",
                success: function(response) {
                    console.log(response);
                    if (response.flag == true) {
                        table.row($("[name=" + 'p' + id + "]"))
                            .remove()
                            .draw();
                    }
                }
            });
        });

        function resetForm() {
            $("[name='stuid']").val('');
            $("[name='name']").val('');
            $("[name='gender']").val('');
            $("[name='mail']").val('');
            $("[name='number']").val('');
            $("[name='place']").val('');
            $("[name='date']").val('');
        }
        $("#insert_faculty").submit(function(e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                type: "POST",
                url: "php/insertfaculty.php",
                data: data,
                dataType: "JSON",
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response);
                    if (response.flag == true) {
                        $("#error").addClass("alert alert-success");
                        $("#error").text("Inserted");
                        resetForm();
                        location.reload(true);
                    } else {
                        $("#error").addClass("alert alert-danger");
                        $("#error").text("Failed");
                    }
                }

            });

        });
        $(document).ready(function() {
            var baseid;
            $("#modalbutton").click(function(e) {
                $("#modelId").modal("show");
            });
            $('[name=update]').click(function(e) {
                e.preventDefault();
                var id = $(this).attr('id');
                baseid = id;
                var name = $("[name=" + 'p' + id + "] #tname").text();
                var mail = $("[name=" + 'p' + id + "] #tmail").text();
                var dept = $("[name=" + 'p' + id + "] #tdept").text();
                var designation = $("[name=" + 'p' + id + "] #tdesc").text();
                var expertise = $("[name=" + 'p' + id + "] #texpt").text();
                var gender = $("[name=" + 'p' + id + "] #tgender").val();
                var place = $("[name=" + 'p' + id + "] #tlocation").val();
                var number = $("[name=" + 'p' + id + "] #tnumber").val();
                var date = $("[name=" + 'p' + id + "] #tdob").val();
                $("#updatefaculty #facid").val(id);
                $("#updatefaculty #name").val(name);
                $("#updatefaculty #mail").val(mail);
                $("#updatefaculty #date").val(date);
                $("#updatefaculty #gender").val(gender);
                $("#updatefaculty #place").val(place);
                $("#updatefaculty #number").val(number);
                $("#updatefaculty #dept").val(dept);
                $("#updatefaculty #designation").val(designation);
                $("#updatefaculty #expt").val(expertise);
                $("#updatefaculty").modal("show");
            });
            $("#update_faculty").submit(function(e) {
                e.preventDefault();
                var data = {
                    'baseid': baseid,
                    'uid': $("#updatefaculty #facid").val(),
                    'name': $("#updatefaculty #name").val(),
                    'mail': $("#updatefaculty #mail").val(),
                    'date': $("#updatefaculty #date").val(),
                    'dept': $("#updatefaculty #dept").val(),
                    'designation': $("#updatefaculty #designation").val(),
                    'expt': $("#updatefaculty #expt").val(),
                    'gender': $("#updatefaculty #gender").val(),
                    'location': $("#updatefaculty #place").val(),
                    'number': $("#updatefaculty #number").val()
                }
                $.ajax({
                    type: "post",
                    url: "php/admin.facultyupdate.php",
                    data: data,
                    dataType: "JSON",
                    success: function(response) {
                        console.log(response);
                        if (response.status == true) {
                            var id = data.baseid;
                            $("#updatefaculty #error").addClass("alert alert-success");
                            $("#updatefaculty #error").text("Updated");
                            $("[name=" + 'p' + id + "] #tname").text(data.name);
                            $("[name=" + 'p' + id + "] #tmail").text(data.mail);
                            $("[name=" + 'p' + id + "] #tdob").text(data.date);
                            $("[name=" + 'p' + id + "] #tdept").text(data.dept);
                            $("[name=" + 'p' + id + "] #tdesc").text(data.designation);
                            $("[name=" + 'p' + id + "] #texpt").text(data.expt);
                            $("[name=" + 'p' + id + "] #tgender").val(data.gender);
                            $("[name=" + 'p' + id + "] #tlocation").val(data.location);
                            $("[name=" + 'p' + id + "] #tnumber").val(data.number);
                            if (data.baseid != data.uid) {
                                location.reload(true);
                            }
                            $("#updatefaculty").modal("hide");
                        } else {
                            $("#updatefaculty #error").addClass("alert alert-danger");
                            $("#updatefaculty #error").text("Failed");
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>